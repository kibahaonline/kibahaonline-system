<?php


class Employee_more_details extends CI_Controller
{
    public function index($id)
    {

        $data['getEmployee'] = $this->CashSubmit->getEmployee($id);
        $data['getEmployeeData'] = $this->CashSubmit->getEmployeeData($id);
        $data['getAdvanceSalary'] = $this->CashSubmit->getAdvanceSalary($id);
        $data['getDebit'] = $this->CashSubmit->getDebit($id);
        $data['getPaySalary'] = $this->CashSubmit->getPaySalary($id);
        $data['getLoan'] = $this->CashSubmit->getLoan($id);
      //  var_dump($data);
      //  return;
        $this->load->view('manage/employee_more_details',$data);
    }
    public function addAdvanceSalary()
    {

        $data['amount'] = $this->input->post('amount', TRUE);
        $data['date'] = $this->input->post('date', TRUE);
        $data['manage_employee_id'] = $this->input->post('manage_employee_id', TRUE);
        $data['delete'] = $this->input->post('delete', TRUE);
        $this->CashSubmit->addAdvanceSalaryData($data);

        redirect("Employee_more_details/index/".$data['manage_employee_id']);

    }
    public function addDebit()
    {

        $data['amount'] = $this->input->post('amount', TRUE);
        $data['date'] = $this->input->post('date', TRUE);
        $data['manage_employee_id'] = $this->input->post('manage_employee_id', TRUE);
        $data['delete'] = $this->input->post('delete', TRUE);
        $data['description'] = $this->input->post('description', TRUE);
        $this->CashSubmit->addDebitData($data);

        redirect("Employee_more_details/index/".$data['manage_employee_id']);

    }

    public function addLoan()
    {

        $data['amount'] = $this->input->post('amount', TRUE);
        $data['date'] = $this->input->post('date', TRUE);
        $data['manage_employee_id'] = $this->input->post('manage_employee_id', TRUE);
        $data['delete'] = $this->input->post('delete', TRUE);
        $data['deduction'] = $this->input->post('deduction', TRUE);
        $data['description'] = $this->input->post('description', TRUE);
        $this->CashSubmit->addLoanData($data);
        redirect("Employee_more_details/index/".$data['manage_employee_id']);

    }

    public function addPaySalary()
    {

        $data['amount'] = $this->input->post('amount', TRUE);
        $data['date'] = $this->input->post('date', TRUE);
        $data['manage_employee_id'] = $this->input->post('manage_employee_id', TRUE);
        $data['delete'] = $this->input->post('delete', TRUE);
        $this->CashSubmit->addPaySalaryData($data);

        redirect("Employee_more_details/index/".$data['manage_employee_id']);

    }
}