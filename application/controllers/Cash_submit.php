<?php


class Cash_submit extends CI_Controller
{

    public function index()
    {
        $data['getSales'] = $this->CashSubmit->getSales();
        $data['getExpenses'] = $this->CashSubmit->getExpenses();

        $this->load->view('manage/cash_submit.php',$data);
    }


    public function addCashSubmit()
    {

        $data['amount'] = $this->input->post('amount', TRUE);
        $data['date'] = $this->input->post('date', TRUE);
        $data['delete'] = $this->input->post('delete', TRUE);
        $this->CashSubmit->addCashData($data);

        redirect("Cash_submit");

    }


}