<?php


class Debtors_creditors extends CI_Controller
{
    public function index()
    {
        $data['getClients'] = $this->CashSubmit->getClients();
        $this->load->view('manage/debtors_creditors.php',$data);
    }
    public function addClient()
    {

        $data['name'] = $this->input->post('name', TRUE);
        $data['delete'] = $this->input->post('delete', TRUE);
        $this->CashSubmit->addClientData($data);

        redirect("Debtors_creditors");

    }
}