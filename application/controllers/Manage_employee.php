<?php


class Manage_employee extends CI_Controller
{
    public function index()
    {
        $data['getEmployees'] = $this->CashSubmit->getEmployees();
        $data['getAllEmployeesData'] = $this->CashSubmit->getAllEmployeesData();
        $this->load->view('manage/manage_employee.php',$data);
    }
    public function addEmployee()
    {

        $data['name'] = $this->input->post('name', TRUE);
        $data['salary_amount'] = $this->input->post('salary_amount', TRUE);
        $data['delete'] = $this->input->post('delete', TRUE);
        $this->CashSubmit->addEmployeeData($data);

        redirect("Manage_employee");

    }
}