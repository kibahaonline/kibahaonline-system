<?php


class Debtors_creditors_more_details extends CI_Controller
{
    public function index($id)
    {
        $data['getClients'] = $this->CashSubmit->getClients($id);
        $data['getClient'] = $this->CashSubmit->getClient($id);
        //$data['getClientPayment'] = $this->CashSubmit->getClientPayment($id);
        $data['getPayment'] = $this->CashSubmit->getPayment($id);
        $data['getPaymentDr'] = $this->CashSubmit->getPaymentDr($id);
        $data['getPaymentCr'] = $this->CashSubmit->getPaymentCr($id);
        //$data['getTotalPayment'] = $this->CashSubmit->getTotalPayment($id);
        $this->load->view('manage/debtors_creditors_more_details.php',$data);
    }
    public function addPayment()
    {

        $data['client_id'] = $this->input->post('client_id', TRUE);
        $data['payment_id'] = $this->input->post('payment_id', TRUE);
        $data['balance'] = $this->input->post('balance', TRUE);
        $data['description'] = $this->input->post('description', TRUE);
        $data['amount'] = $this->input->post('amount', TRUE);
        $data['delete'] = $this->input->post('delete', TRUE);
        $data['date'] = $this->input->post('date', TRUE);
        $this->CashSubmit->addPaymentData($data);
        redirect("Debtors_creditors_more_details/index/".$data['client_id']);

    }
    public function addPaymentDr()
    {
        $data['client_id'] = $this->input->post('client_id', TRUE);
        $data['dr'] = $this->input->post('dr', TRUE);
        $data['balance'] = $this->input->post('balance', TRUE);
        $data['description'] = $this->input->post('description', TRUE);
        $data['cr'] = $this->input->post('cr', TRUE);
        $data['delete'] = $this->input->post('delete', TRUE);
        $data['date'] = $this->input->post('date', TRUE);
        $this->CashSubmit->addPaymentDrData($data);
        redirect("Debtors_creditors_more_details/index/".$data['client_id']);

    }
    public function addPaymentCr()
    {
        $data['client_id'] = $this->input->post('client_id', TRUE);
        $data['cr'] = $this->input->post('cr', TRUE);
        $data['balance'] = $this->input->post('balance', TRUE);
        $data['description'] = $this->input->post('description', TRUE);
        $data['dr'] = $this->input->post('dr', TRUE);
        $data['delete'] = $this->input->post('delete', TRUE);
        $data['date'] = $this->input->post('date', TRUE);
        $this->CashSubmit->addPaymentCrData($data);
        redirect("Debtors_creditors_more_details/index/".$data['client_id']);

    }
}