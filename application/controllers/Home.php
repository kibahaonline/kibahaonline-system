<?php


class Home extends CI_Controller
{
    public function index()
    {
        $data['getExpensesToday'] = $this->CashSubmit->getExpensesToday();
        $this->load->view('home.php',$data);
    }
}