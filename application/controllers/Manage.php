<?php


class Manage extends CI_Controller
{
    public function index()
    {
        $data['getExpensesToday'] = $this->CashSubmit->getExpensesToday();
        $data['getSalesToday'] = $this->CashSubmit->getSalesToday();
        $this->load->view('manage/index.php',$data);
    }
}