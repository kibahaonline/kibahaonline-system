<?php


class CashSubmit extends CI_Model
{
    function addCashData($data){
        $insert_data['amount'] = $data['amount'];
        $insert_data['date'] = $data['date'];
        $insert_data['delete'] = $data['delete'];
        $query = $this->db->insert('ospos_cash_submit', $insert_data);
    }

    function addEmployeeData($data){
        $insert_data['name'] = $data['name'];
        $insert_data['salary_amount'] = $data['salary_amount'];
        $insert_data['delete'] = $data['delete'];
        $query = $this->db->insert('ospos_manage_employees', $insert_data);
    }
    function addClientData($data){
        $insert_data['name'] = $data['name'];
        $insert_data['delete'] = $data['delete'];
        $query = $this->db->insert('ospos_clients', $insert_data);
    }
    function addAdvanceSalaryData($data){
        $insert_data['manage_employee_id'] = $data['manage_employee_id'];
        $insert_data['amount'] = $data['amount'];
        $insert_data['delete'] = $data['delete'];
        $insert_data['date'] = $data['date'];
        $query = $this->db->insert('ospos_advance_salary', $insert_data);
    }
    function addDebitData($data){
        $insert_data['manage_employee_id'] = $data['manage_employee_id'];
        $insert_data['amount'] = $data['amount'];
        $insert_data['delete'] = $data['delete'];
        $insert_data['date'] = $data['date'];
        $insert_data['description'] = $data['description'];
        $query = $this->db->insert('ospos_debit', $insert_data);
    }
    function addLoanData($data){
        $insert_data['manage_employee_id'] = $data['manage_employee_id'];
        $insert_data['amount'] = $data['amount'];
        $insert_data['delete'] = $data['delete'];
        $insert_data['date'] = $data['date'];
        $insert_data['deduction'] = $data['deduction'];
        $insert_data['description'] = $data['description'];
        $query = $this->db->insert('ospos_loan', $insert_data);
    }
    function addPaySalaryData($data){
        $insert_data['manage_employee_id'] = $data['manage_employee_id'];
        $insert_data['amount'] = $data['amount'];
        $insert_data['delete'] = $data['delete'];
        $insert_data['date'] = $data['date'];
        $query = $this->db->insert('ospos_salary', $insert_data);
    }
    function addPaymentData($data){
        $insert_data['client_id'] = $data['client_id'];
        $insert_data['payment_id'] = $data['payment_id'];
        $insert_data['balance'] = $data['balance'];
        $insert_data['description'] = $data['description'];
        $insert_data['amount'] = $data['amount'];
        $insert_data['delete'] = $data['delete'];
        $insert_data['date'] = $data['date'];
        $query = $this->db->insert('ospos_debits_credits', $insert_data);
    }
    function addPaymentDrData($data){
        $insert_data['client_id'] = $data['client_id'];
        $insert_data['dr'] = $data['dr'];
        $insert_data['balance'] = $data['balance'];
        $insert_data['description'] = $data['description'];
        $insert_data['cr'] = $data['cr'];
        $insert_data['delete'] = $data['delete'];
        $insert_data['date'] = $data['date'];
        $query = $this->db->insert('ospos_debits_credits', $insert_data);
    }
    function addPaymentCrData($data){
        $insert_data['client_id'] = $data['client_id'];
        $insert_data['dr'] = $data['dr'];
        $insert_data['balance'] = $data['balance'];
        $insert_data['description'] = $data['description'];
        $insert_data['cr'] = $data['cr'];
        $insert_data['delete'] = $data['delete'];
        $insert_data['date'] = $data['date'];
        $query = $this->db->insert('ospos_debits_credits', $insert_data);
    }

    function getSales(){
        $sales =   $this->db->query("SELECT sum(i.quantity_purchased*i.item_unit_price) AS sales, 
                                     DATE(s.sale_time) as date 
                                     FROM ospos_sales_items i,ospos_sales s 
                                     where i.sale_id=s.sale_id
                                     GROUP BY DATE(s.sale_time) order by DATE(s.sale_time) DESC ");
        return $sales->result();
    }

    function getSalesToday(){
        $sales =   $this->db->query("SELECT sum(i.quantity_purchased*i.item_unit_price) AS sales, 
                                     DATE(s.sale_time) as date 
                                     FROM ospos_sales_items i,ospos_sales s 
                                     where i.sale_id=s.sale_id AND DATE(s.sale_time)=CURDATE() GROUP BY DATE(s.sale_time) ");
        return $sales->result();
    }

    function getExpenses(){
        $expenses =   $this->db->query("SELECT sum(amount) as expenses,DATE(date) 
                                        FROM ospos_expenses GROUP BY DATE(date)   ");
        return $expenses->result();
    }

    function getExpensesToday(){
        $expenses =   $this->db->query("SELECT sum(amount) as expenses,DATE(date) 
                                        FROM ospos_expenses WHERE date=CURDATE() ;  ");
        return $expenses->result();
    }


    function gettotalexp_per_day($date){
        $this->db->select_sum('amount');
        $this->db->like('date',$date);
        return $this->db->get('ospos_expenses')->row('amount');
    }
    function getTotalCredit_per_day($date){
        $this->db->select_sum('cr');
        $this->db->like('date',$date);
        return $this->db->get('ospos_debits_credits')->row('cr');
    }
    function getTotalDebit_per_day($date){
        $this->db->select_sum('dr');
        $this->db->like('date',$date);
        return $this->db->get('ospos_debits_credits')->row('dr');
    }
    function getTotaladvancesalary_per_day($date){
        $this->db->select_sum('amount');
        $this->db->like('date',$date);
        return $this->db->get('ospos_advance_salary')->row('amount');
    }
    function getTotalCashSubmitted_per_day($date){
        $this->db->select_sum('amount');
        $this->db->like('date',$date);
        return $this->db->get('ospos_cash_submit')->row('amount');
    }

    ///


    function getEmployees(){
        $expenses =   $this->db->query("SELECT * FROM ospos_manage_employees   ");
        return $expenses->result();
    }
    function getClients(){
        $expenses =   $this->db->query("SELECT *,
                                        (SELECT SUM(cr) FROM ospos_debits_credits WHERE client_id=ospos_clients.id) as credit,
                                        (SELECT SUM(dr) FROM ospos_debits_credits WHERE client_id=ospos_clients.id) as debit
                                    
                                        FROM ospos_clients");
        return $expenses->result();
    }
    function getEmployeeData($id){
        $expenses =   $this->db->query("SELECT *,
(SELECT sum(amount) FROM `ospos_advance_salary` WHERE `manage_employee_id`= $id) as advance_salary, 
(SELECT sum(amount) FROM `ospos_debit` WHERE `manage_employee_id`= $id) as debit,
(SELECT sum(amount) FROM `ospos_salary` WHERE `manage_employee_id`= $id) as salary,
(SELECT sum(amount) FROM `ospos_loan` WHERE `manage_employee_id`= $id) as loan,
(SELECT sum(deduction) FROM `ospos_loan` WHERE `manage_employee_id`= $id) as deduction
FROM `ospos_manage_employees` WHERE id=$id   ");
        return $expenses->result();
    }
    function getAllEmployeesData(){
        $expenses =   $this->db->query("SELECT *,
(SELECT sum(amount) FROM `ospos_advance_salary` WHERE `manage_employee_id`= `ospos_manage_employees`.id) as advance_salary, 
(SELECT sum(amount) FROM `ospos_debit` WHERE `manage_employee_id`= `ospos_manage_employees`.id) as debit,
(SELECT sum(amount) FROM `ospos_salary` WHERE `manage_employee_id`= `ospos_manage_employees`.id) as salary,
(SELECT sum(amount) FROM `ospos_loan` WHERE `manage_employee_id`= `ospos_manage_employees`.id) as loan,
(SELECT sum(deduction) FROM `ospos_loan` WHERE `manage_employee_id`= `ospos_manage_employees`.id) as deduction
FROM `ospos_manage_employees` WHERE id=`ospos_manage_employees`.id   ");
        return $expenses->result();
    }
    function getEmployee($id){
        $expenses =   $this->db->query("SELECT * FROM ospos_manage_employees where id=$id ");
        return $expenses->result();
    }
    function getClient($id){
        $expenses =   $this->db->query("SELECT * FROM ospos_clients where id=$id ");
        return $expenses->result();
    }
    function getPayment($id){
        $expenses =   $this->db->query("SELECT id, client_id, date, cr, dr,description,
COALESCE(((SELECT SUM(cr) FROM ospos_debits_credits b WHERE b.id <= a.id AND client_id = '$id') - (SELECT SUM(dr) FROM ospos_debits_credits b WHERE b.id <= a.id AND client_id = '$id')), 0) as balance
FROM ospos_debits_credits a WHERE client_id = '$id' ORDER BY id DESC");
        return $expenses->result();
    }
    function getPaymentDr($id){
        $expenses =   $this->db->query("SELECT * FROM ospos_debits_credits WHERE dr != 0 AND client_id= $id ORDER BY id DESC");
        return $expenses->result();
    }
    function getPaymentCr($id){
        $expenses =   $this->db->query("SELECT * FROM ospos_debits_credits WHERE cr != 0 AND client_id= $id ORDER BY id DESC");
        return $expenses->result();
    }

    /*function getTotalPayment(){
        $expenses =   $this->db->query("SELECT *,(SELECT sum(cr) FROM ospos_debits_credits )  FROM ospos_debits_credits ");
        return $expenses->result();
    }*/

    /*function getClientPayment($id){
        $expenses =   $this->db->query("SELECT *,(SELECT sum(amount) FROM ospos_debits_credits WHERE payment_id = 1) as dr,
                                        (SELECT (amount) FROM ospos_debits_credits WHERE payment_id = 2) as cr
                                        FROM ospos_clients where id=$id ");
        return $expenses->result();
    }*/
    function getAdvanceSalary($id){
        $expenses =   $this->db->query("SELECT * FROM ospos_advance_salary where manage_employee_id=$id ");
        return $expenses->result();
    }
    function getDebit($id){
        $expenses =   $this->db->query("SELECT * FROM ospos_debit where manage_employee_id=$id ");
        return $expenses->result();
    }
    function getPaySalary($id){
        $expenses =   $this->db->query("SELECT * FROM ospos_salary where manage_employee_id=$id ");
        return $expenses->result();
    }
    function getLoan($id){
        $expenses =   $this->db->query("SELECT * FROM ospos_loan where manage_employee_id=$id ");
        return $expenses->result();
    }
}