<!DOCTYPE html>
<html>
<?php $this->load->view("manage/header"); ?>


<body>

<!-- Loader -->
<div id="preloader"><div id="status"><div class="spinner"></div></div></div>

<?php $this->load->view("manage/topnav"); ?>


<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">KibahaOnline</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Dashboard</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->
        <div class="row">
            <!-- Column -->
            <div class="col-md-6 col-lg-6 col-xl-3">
                <div class="card m-b-30">
                    <div class="card-body">
                        <div class="d-flex flex-row">
                            <div class="col-3 align-self-center">
                                <div class="round">
                                    <i class="mdi mdi-webcam"></i>
                                </div>
                            </div>
                            <div class="col-9 align-self-center text-center">
                                <div class="m-l-10">
                                    <h5 class="mt-0 round-inner">235,000</h5>
                                    <p class="mb-0 text-muted">Today Sales</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-md-6 col-lg-6 col-xl-3">
                <div class="card m-b-30">
                    <div class="card-body">
                        <div class="d-flex flex-row">
                            <div class="col-3 align-self-center">
                                <div class="round">
                                    <i class="mdi mdi-account-multiple-plus"></i>
                                </div>
                            </div>
                            <div class="col-9 text-center align-self-center">
                                <div class="m-l-10 ">
                                    <h5 class="mt-0 round-inner">890,000</h5>
                                    <p class="mb-0 text-muted">Today Expenses</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-md-6 col-lg-6 col-xl-3">
                <div class="card m-b-30">
                    <div class="card-body">
                        <div class="d-flex flex-row">
                            <div class="col-3 align-self-center">
                                <div class="round ">
                                    <i class="mdi mdi-basket"></i>
                                </div>
                            </div>
                            <div class="col-9 align-self-center text-center">
                                <div class="m-l-10 ">
                                    <h5 class="mt-0 round-inner">50,000</h5>
                                    <p class="mb-0 text-muted">Employees Credit</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-md-6 col-lg-6 col-xl-3">
                <div class="card m-b-30">
                    <div class="card-body">
                        <div class="d-flex flex-row">
                            <div class="col-3 align-self-center">
                                <div class="round">
                                    <i class="mdi mdi-rocket"></i>
                                </div>
                            </div>
                            <div class="col-9 align-self-center text-center">
                                <div class="m-l-10">
                                    <h5 class="mt-0 round-inner">20,000</h5>
                                    <p class="mb-0 text-muted">Today Loss/Gain</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl-12 align-self-center">
                <div class="card bg-white m-b-30">
                    <div class="card-body new-user">
                        <h5 class="header-title mb-4 mt-0">Employee Balance Account</h5>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th class="border-top-0" style="width:60px;">Member</th>
                                    <th class="border-top-0">Name</th>
                                    <th class="border-top-0">Salary</th>
                                    <th class="border-top-0">Credit</th>
                                    <th class="border-top-0">Amount Paid</th>
                                    <th class="border-top-0">Balance</th>
                                </tr>
                                </thead>
                                <tbody>

                                <tr>
                                    <td>
                                        <img class="rounded-circle" src="<?=base_url('')?>assets/images/users/avatar-3.jpg" alt="user" width="40"> </td>
                                    <td>
                                        <a href="javascript:void(0);">William A. Johnson</a>
                                    </td>
                                    <td>
                                        150,000
                                    </td>
                                    <td>50,000</td>
                                    <td class="text-center">0</td>
                                    <td>100,000</td>
                                </tr>

                                </tbody>
                                <tfoot>
                                <tr>
                                    <td>

                                    <td>
                                        Total
                                    </td>
                                    <td>
                                        150,000
                                    </td>
                                    <td>50,000</td>
                                    <td class="text-center">0</td>
                                    <td>100,000</td>
                                </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div> <!-- end container -->
</div>
<!-- end wrapper -->


<?php $this->load->view("manage/footer"); ?>