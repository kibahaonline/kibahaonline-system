<!DOCTYPE html>
<html>
<?php $this->load->view("manage/header"); ?>


<body>

<!-- Loader -->
<div id="preloader"><div id="status"><div class="spinner"></div></div></div>

<?php $this->load->view("manage/topnav"); ?>


<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">KibahaOnline</a></li>
                            <li class="breadcrumb-item active">Employees</li>
                        </ol>
                    </div>
                    <?php
                     foreach ($getEmployee as $index => $item) {
                         ?>
                         <h4 class="page-title"><?=$item->name;?> Account's</h4>

                         <?php
                     }
 ?>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->
        <?php


        foreach ($getEmployeeData as $index => $item) {
        //var_dump($item);
        $id = $item->id;
        $net_salary_new = ($item->salary_amount)-($item->advance_salary);
        $deduction_new=$item->deduction;
        $balance_loan_new=($item->loan)-($item->debit);
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="card m-b-30">
                    <div class="card-body">
                        <div class="text-right">
                            <?php
                            if($net_salary_new>=$deduction_new) {
                                ?>
                                <button type="button" class="btn btn-primary waves-effect waves-light "
                                        data-toggle="modal" data-animation="bounce"
                                        data-target=".bs-example-modal-center-advance-salary">Add Advance Salary
                                </button>
                                <?php
                            }
 ?>
                            <button type="button" class="btn btn-primary waves-effect waves-light " data-toggle="modal" data-animation="bounce" data-target=".bs-example-modal-center-debit">Add Debit</button>
                            <button type="button" class="btn btn-primary waves-effect waves-light " data-toggle="modal" data-animation="bounce" data-target=".bs-example-modal-center-pay-salary">Pay Salary</button>
                            <?php
                            if($balance_loan_new<1){
                                ?>


                            <button type="button" class="btn btn-primary waves-effect waves-light " data-toggle="modal" data-animation="bounce" data-target=".bs-example-modal-center-loan">Add Loan</button>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <?php
        }
        ?>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="card m-b-30">
                    <div class="card-body">
                        <!-- Nav tabs -->

                                            <ul class="nav nav-pills nav-justified" role="tablist">
                                                <li class="nav-item waves-effect waves-light">
                                                    <a class="nav-link active" data-toggle="tab" href="#home-1"
                                                       role="tab">Home</a>
                                                </li>
                                                <li class="nav-item waves-effect waves-light">
                                                    <a class="nav-link" data-toggle="tab" href="#profile-1" role="tab">Advance
                                                        Salary</a>
                                                </li>
                                                <li class="nav-item waves-effect waves-light">
                                                    <a class="nav-link" data-toggle="tab" href="#messages-1" role="tab">Debit</a>
                                                </li>
                                                <li class="nav-item waves-effect waves-light">
                                                    <a class="nav-link" data-toggle="tab" href="#settings-1" role="tab">Pay
                                                        Salary</a>
                                                </li>
                                                <li class="nav-item waves-effect waves-light">
                                                    <a class="nav-link" data-toggle="tab" href="#settings-2" role="tab">Loan</a>
                                                </li>
                                            </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active p-3" id="home-1" role="tabpanel">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Employee Name</th>
                                        <th>Basic Salary</th>
                                        <th>Salary Advance</th>
                                        <th>Debit</th>
                                        <th>Loan Monthly Deduction</th>
                                        <th>Net Salary</th>
                                        <th>Total Loan</th>
                                        <th>Loan Balance</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                  //  var_dump($getEmployee);
                                    if(isset($getEmployeeData)){
                                        $sn=1;
                                        $salary=0;
                                        $total_salary=0;
                                        foreach ($getEmployeeData as $index => $item) {
                                            //var_dump($item);
                                            $id=$item->id;
                                            $salary=$item->salary_amount;
                                            $total_salary=$total_salary+$salary;
                                            ?>
                                            <tr>
                                                <th scope="row"><?= $sn;?></th>
                                                <td ><?= $item->name;?></td>
                                                <td class="text-right"><?= number_format($item->salary_amount);?></td>
                                                <td class="text-right"><?= number_format($item->advance_salary);?></td>
                                                <td class="text-right"><?= number_format($item->debit);?></td>
                                                <td class="text-right"><?= number_format($item->deduction);?></td>
                                                <td class="text-right"><?= number_format(($item->salary_amount)-($item->advance_salary));?></td>
                                                <td class="text-right"><?= number_format($item->loan);?></td>
                                                <td class="text-right"><?= number_format(($item->loan)-($item->debit));?></td>
                                                <td style="width: 100px;"><i class="fa fa-pencil-square-o btn-primary" data-toggle="tooltip" data-placement="top" title="edit"></i> &nbsp;&nbsp;
                                                   <i class="fa fa-trash badge-danger" data-toggle="tooltip" data-placement="top" title="delete"></i></td>

                                            </tr>

                                            <!--         --><?php
                                            $sn++;}
                                        ?>



                                    </tbody>

                                    <?php
                                    }
                                    ?>
                                </table>
                            </div>
                            <div class="tab-pane p-3" id="profile-1" role="tabpanel">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Date</th>
                                        <th>Amount</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                     // var_dump($getAdvanceSalary);
                                    if(isset($getAdvanceSalary)){
                                        $sn=1;
                                        $advance_salary=0;
                                        $total=0;
                                        foreach ($getAdvanceSalary as $index => $item) {
                                            //var_dump($item);
                                            $id=$item->id;
                                            $advance_salary=$item->amount;
                                            $total=$total+$advance_salary;
                                            ?>



                                            <tr>
                                                <th scope="row"><?= $sn;?></th>
                                                <td><?= $item->date;?></td>
                                                <td class="text-right"><?= number_format($advance_salary);?></td>

                                                <td style="width: 100px;"><i class="fa fa-pencil-square-o btn-primary" data-toggle="tooltip" data-placement="top" title="edit"></i>&nbsp;&nbsp;&nbsp;<i class="fa fa-trash badge-danger" data-toggle="tooltip" data-placement="top" title="delete"></i></td>

                                            </tr>

                                            <!--         --><?php
                                            $sn++;}
                                    }
                                    ?>
                                    </tbody>
                                    <tfoot>
                                    <tr class="text-right">
                                        <th></th>
                                        <th>TOTAL</th>
                                        <th><?= number_format($total);?></th>
                                        <th></th>

                                    </tr>
                                    </tfoot>
                                </table>

                            </div>
                            <div class="tab-pane p-3" id="messages-1" role="tabpanel">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Date</th>
                                        <th>Description</th>
                                        <th>Amount</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    // var_dump($getAdvanceSalary);
                                    if(isset($getDebit)){
                                        $sn=1;
                                        $debit=0;
                                        $total=0;
                                        foreach ($getDebit as $index => $item) {
                                            //var_dump($item);
                                            $id=$item->id;
                                            $debit=$item->amount;
                                            $total=$total+$debit;
                                            ?>



                                            <tr>
                                                <th scope="row"><?= $sn;?></th>
                                                <td><?= $item->date;?></td>
                                                <td><?= $item->description;?></td>
                                                <td class="text-right"><?= number_format($debit);?></td>

                                                <td style="width: 100px;"><i class="fa fa-pencil-square-o btn-primary" data-toggle="tooltip" data-placement="top" title="edit"></i>
                                                   &nbsp;
                                                    <i class="fa fa-trash badge-danger" data-toggle="tooltip" data-placement="top" title="delete"></i></td>

                                            </tr>

                                            <!--         --><?php
                                            $sn++;}
                                    }
                                    ?>
                                    </tbody>
                                    <tfoot>
                                    <tr class="text-right">
                                        <th></th>
                                        <th>TOTAL</th>
                                        <th></th>
                                        <th><?= number_format($total);?></th>

                                        <th></th>

                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="tab-pane p-3" id="settings-1" role="tabpanel">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Date</th>
                                        <th>Amount</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    // var_dump($getAdvanceSalary);
                                    if(isset($getPaySalary)){
                                        $sn=1;
                                        $pay_salary=0;
                                        $total=0;
                                        foreach ($getPaySalary as $index => $item) {
                                            //var_dump($item);
                                            $id=$item->id;
                                            $pay_salary=$item->amount;
                                            $total=$total+$pay_salary;
                                            ?>



                                            <tr>
                                                <th scope="row"><?= $sn;?></th>
                                                <td><?= $item->date;?></td>
                                                <td class="text-right"><?= number_format($pay_salary);?></td>

                                                <td style="width: 100px;"><i class="fa fa-pencil-square-o btn-primary" data-toggle="tooltip" data-placement="top" title="edit"></i>
                                                    <i class="fa fa-trash badge-danger" data-toggle="tooltip" data-placement="top" title="delete"></i></td>

                                            </tr>

                                            <!--         --><?php
                                            $sn++;}
                                    }
                                    ?>
                                    </tbody>
                                    <tfoot>
                                    <tr class="text-right">
                                        <th></th>
                                        <th>TOTAL</th>
                                        <th><?= number_format($total);?></th>
                                        <th></th>

                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="tab-pane p-3" id="settings-2" role="tabpanel">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Date</th>
                                        <th>description</th>
                                        <th>Loan Amount</th>
                                        <th>Monthly Deduction</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    // var_dump($getAdvanceSalary);
                                    if(isset($getLoan)){
                                        $sn=1;
                                        $loan=0;
                                        $total=0;
                                        foreach ($getLoan as $index => $item) {
                                            //var_dump($item);
                                            $id=$item->id;
                                            $loan=$item->amount;
                                            $total=$total+$loan;
                                            ?>



                                            <tr>
                                                <th scope="row"><?= $sn;?></th>
                                                <td><?= $item->date;?></td>
                                                <td><?= $item->description;?></td>
                                                <td class="text-right"><?= number_format($loan);?></td>
                                                <td class="text-right"><?= number_format($item->deduction);?></td>

                                                <td style="width: 100px;"><i class="fa fa-pencil-square-o btn-primary" data-toggle="tooltip" data-placement="top" title="edit"></i>
                                                    <i class="fa fa-trash badge-danger" data-toggle="tooltip" data-placement="top" title="delete"></i></td>

                                            </tr>

                                            <!--         --><?php
                                            $sn++;}
                                    }
                                    ?>
                                    </tbody>
                                    <tfoot>
                                    <tr class="text-right">
                                        <th></th>
                                        <th>TOTAL</th>
                                        <th></th>
                                        <th><?= number_format($total);?></th>
                                        <th></th>
                                        <th></th>

                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div> <!-- end container -->
</div>
<div class="modal fade bs-example-modal-center-advance-salary" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Add Advance Salary</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="<?php echo site_url('Employee_more_details/addAdvanceSalary'); ?>" method="post" name="save" enctype="multipart/form-data">
                    <div class="form-group row">
                        <div class="col-sm-8">
                            <input class="form-control" type="hidden" id="manage_employee_id" name="manage_employee_id" value="<?php
                            foreach ($getEmployee as $index => $item) {
                                echo $item->id;
                            }

                            ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-date-input" class="col-sm-6 col-form-label">Date</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <input type="text" class="form-control mdate"  name="date">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="salary_amount" class="col-sm-6 col-form-label">Advance Salary Amount</label>
                        <div class="col-sm-6">
                            <input class="form-control" type="number" id="amount" name="amount">
                        </div>
                    </div><!--
                    -->
                    <input type="hidden" class="form-control"  id="delete" name="delete" value="0">
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" value="save">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>







            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade bs-example-modal-center-debit" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Add Debit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="<?php echo site_url('Employee_more_details/addDebit'); ?>" method="post" name="save" enctype="multipart/form-data">
                    <div class="form-group row">
                        <div class="col-sm-8">
                            <input class="form-control" type="hidden" id="manage_employee_id" name="manage_employee_id" value="<?php
                            foreach ($getEmployee as $index => $item) {
                                echo $item->id;
                            }

                            ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-date-input" class="col-sm-6 col-form-label">Date</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <input type="text" class="form-control mdate"   name="date">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="description" class="col-sm-6 col-form-label">Description</label>
                        <div class="col-sm-6">
                            <input class="form-control" type="text" id="description" name="description">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="salary_amount" class="col-sm-6 col-form-label">Debit Amount</label>
                        <div class="col-sm-6">
                            <input class="form-control" type="number" id="amount" name="amount">
                        </div>
                    </div><!--
                    -->
                    <input type="hidden" class="form-control"  id="delete" name="delete" value="0">
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" value="save">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade bs-example-modal-center-pay-salary" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Pay Salary</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="<?php echo site_url('Employee_more_details/addPaySalary'); ?>" method="post" name="save" enctype="multipart/form-data">
                    <div class="form-group row">
                        <div class="col-sm-8">
                            <input class="form-control" type="hidden" id="manage_employee_id" name="manage_employee_id" value="<?php
                            foreach ($getEmployee as $index => $item) {
                                echo $item->id;
                            }

                            ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-date-input" class="col-sm-6 col-form-label">Date</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <input type="text" class="form-control mdate"   name="date">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="salary_amount" class="col-sm-6 col-form-label">Pay Salary Amount</label>
                        <div class="col-sm-6">
                            <input class="form-control" type="number" id="amount" name="amount">
                        </div>
                    </div>
                    <?php
                    //Todo add auto pay salary amount.
                    ?>

                    <!--
                    -->
                    <input type="hidden" class="form-control"  id="delete" name="delete" value="0">
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" value="save">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>







            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade bs-example-modal-center-loan" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Add Loan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="<?php echo site_url('Employee_more_details/addLoan'); ?>" method="post" name="save" enctype="multipart/form-data">
                    <div class="form-group row">
                        <div class="col-sm-8">
                            <input class="form-control" type="hidden" id="manage_employee_id" name="manage_employee_id" value="<?php
                            foreach ($getEmployee as $index => $item) {
                                echo $item->id;
                            }

                            ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-date-input" class="col-sm-6 col-form-label">Date</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <input type="text" class="form-control"  id="mdate" name="date">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="description" class="col-sm-6 col-form-label">Description</label>
                        <div class="col-sm-6">
                            <input class="form-control" type="text" id="description" name="description">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="salary_amount" class="col-sm-6 col-form-label">Loan Amount</label>
                        <div class="col-sm-6">
                            <input class="form-control" type="number" id="amount" name="amount">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="salary_amount" class="col-sm-6 col-form-label">Monthly Deduction Amount</label>
                        <div class="col-sm-6">
                            <input class="form-control" type="number" id="deduction" name="deduction">
                        </div>
                    </div>
                    <input type="hidden" class="form-control"  id="delete" name="delete" value="0">
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" value="save">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>







            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>


<!-- end wrapper -->
<?php $this->load->view("manage/footer"); ?>