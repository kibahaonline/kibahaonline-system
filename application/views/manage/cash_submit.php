<!DOCTYPE html>
<html>
<?php $this->load->view("manage/header"); ?>


<body>

<!-- Loader -->
<div id="preloader"><div id="status"><div class="spinner"></div></div></div>

<?php $this->load->view("manage/topnav"); ?>


<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">KibahaOnline</a></li>
                            <li class="breadcrumb-item active">Cash Submit</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Cash Submit</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card m-b-30">
                    <div class="card-body">
                        <div class="text-right">
                            <button type="button" class="btn btn-primary waves-effect waves-light " data-toggle="modal" data-animation="bounce" data-target=".bs-example-modal-center">Submit Cash</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card m-b-30">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">Cash Submitted Report</h4>
                        <table id="datatable-buttons" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Sales</th>
                                <th>Debit</th>
                                <th>Credit</th>
                                <th>Advance Salary</th>
                                <th>Expenses</th>
                                <th>Cash Amount</th>
                                <th>Cash Submitted</th>
                                <th>Gain/loss</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            if(isset($getSales)){
                                $sn=1;
                                foreach ($getSales as $index => $getSale) {
                                    $sales=$getSale->sales;
                                    $myexp = $this->CashSubmit->gettotalexp_per_day($getSale->date);
                                    $mydebit = $this->CashSubmit->getTotalDebit_per_day($getSale->date);
                                    $mycredit = $this->CashSubmit->getTotalCredit_per_day($getSale->date);
                                    $cash_submitted = $this->CashSubmit->getTotalCashSubmitted_per_day($getSale->date);
                                    $my_advance_salary = $this->CashSubmit->getTotaladvancesalary_per_day($getSale->date);
                                    $cash_amount=($sales+$mydebit)-($mycredit+$my_advance_salary+$myexp);
                                    $gain_loss=$cash_submitted-$cash_amount;
                                    //var_dump($getSale);
                                    ?>



                            <tr>
                                <th scope="row"><?= $sn;?></th>
                                <td><?= $getSale->date; ?></td>
                                <td><?= number_format($getSale->sales,0); ?></td>
                                <td><?= number_format($mydebit);?></td>
                                <td><?= number_format($mycredit);?></td>
                                <td><?= number_format($my_advance_salary);?></td>



                                <td><?= number_format($myexp);?></td>
                                <td><?=number_format($cash_amount);?></td>
                                <td><?=number_format($cash_submitted)?></td>
                                <td><?=number_format($gain_loss);?></td>
                            </tr>

                                    <?php
                                $sn++;}
                            }
                            ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>


    </div> <!-- end container -->
</div>
<!-- end wrapper -->
<div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Add Cash</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="<?php echo site_url('Cash_submit/addCashSubmit'); ?>" method="post" name="save" enctype="multipart/form-data">
                    <div class="form-group row">
                        <label for="amount" class="col-sm-4 col-form-label">Cash Amount</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="number" id="amount" name="amount">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-date-input" class="col-sm-4 col-form-label">Date</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <input type="text" class="form-control mdate"  name="date">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="form-control"  id="delete" name="delete" value="0">
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" value="save">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>







            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<?php $this->load->view("manage/footer"); ?>