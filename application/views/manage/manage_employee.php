<!DOCTYPE html>
<html>
<?php $this->load->view("manage/header"); ?>


<body>

<!-- Loader -->
<div id="preloader"><div id="status"><div class="spinner"></div></div></div>

<?php $this->load->view("manage/topnav"); ?>


<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">KibahaOnline</a></li>
                            <li class="breadcrumb-item active">Employees</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Employees</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card m-b-30">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-10">
                                <div class="general-labelx">
                                    <form class="form-inline" role="form">
                                        <div class="form-group">
                                            <select name="" id="" class="form-control" style="width: 200px;">
                                                <option value="">2019</option>
                                                <option value="" selected>2020</option>
                                                <option value="">2021</option>
                                                <option value="">2022</option>
                                                <option value="">2023</option>
                                            </select>
                                        </div>

                                        <div class="form-group m-l-10">
                                            <select name="" id="" class="form-control" style="width: 200px;">
                                                <option value="">January</option>
                                                <option value="">February</option>
                                                <option value="">March</option>
                                                <option value="" selected>April</option>
                                                <option value="">May</option>
                                                <option value="">June</option>
                                                <option value="">July</option>
                                                <option value="">August</option>
                                                <option value="">September</option>
                                                <option value="">October</option>
                                                <option value="">November</option>
                                                <option value="">December</option>
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-success ml-2">Search</button>
                                    </form>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <button type="button" class="btn btn-primary waves-effect waves-light "
                                        data-toggle="modal" data-animation="bounce"
                                        data-target=".bs-example-modal-center">Register Employee
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card m-b-30">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">Employee Report</h4>
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Employee Name</th>
                                <th>Basic Salary</th>
                                <th>Salary Advance</th>
                                <th>Debit</th>
                                <th>Loan Monthly Deduction</th>
                                <th>Net Salary</th>
                                <th>Total Loan</th>
                                <th>Loan Balance</th>
                                <th>Action</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            if(isset($getAllEmployeesData)){
                                $sn=1;
                                $salary=0;
                                $total_salary=0;
                                $advance=0;
                                $total_advance=0;
                                $debit=0;
                                $total_debit=0;
                                $net_salary=0;
                                $total_net_salary=0;
                                $loan=0;
                                $total_loan=0;
                                $balance=0;
                                $total_balance=0;
                                foreach ($getAllEmployeesData as $index => $getEmployee) {
                                    //var_dump($getSale);
                                    $id=$getEmployee->id;
                                    $salary=$getEmployee->salary_amount;
                                    $total_salary=$total_salary+$salary;
                                    $advance=$getEmployee->advance_salary;
                                    $total_advance=$total_advance+$advance;
                                    $debit=$getEmployee->debit;
                                    $total_debit=$total_debit+$debit;
                                    $net_salary=($getEmployee->salary_amount)-($getEmployee->advance_salary);
                                    $total_net_salary=$total_net_salary+$net_salary;
                                    $loan=$getEmployee->loan;
                                    $total_loan=$total_loan+$loan;
                                    $balance=($getEmployee->loan)-($getEmployee->debit);
                                    $total_balance=$total_balance+$balance;
                                    ?>



                                    <tr>
                                        <th scope="row"><?= $sn;?></th>
                                        <td><?= $getEmployee->name;?></td>
                                        <td class="text-right"><?= number_format($getEmployee->salary_amount);?></td>
                                        <td class="text-right"><?= number_format($getEmployee->advance_salary);?></td>
                                        <td class="text-right"><?= number_format($getEmployee->debit);?></td>
                                        <td class="text-right"><?= number_format($getEmployee->deduction);?></td>

                                        <td class="text-right"><?= number_format(($getEmployee->salary_amount)-($getEmployee->advance_salary));?></td>
                                        <td class="text-right"><?= number_format($getEmployee->loan);?></td>
                                        <td class="text-right"><?= number_format(($getEmployee->loan)-($getEmployee->debit));?></td>

                                        <td style="width: 100px;" class="text-center">
                                            <a href="<?= base_url('Employee_more_details/index/'.$id); ?>"><i class="fa fa-eye btn-success" data-toggle="tooltip" data-placement="top" title="view"></i></a>&nbsp;&nbsp;&nbsp;
                                           </td>

                                    </tr>

                           <!--         --><?php
                                    $sn++;}
                            }
                            ?>
                            </tbody>
                            <tfoot>
                                <tr class="text-right">
                                    <th></th>
                                    <th>TOTAL</th>
                                    <th><?= number_format($total_salary);?></th>
                                    <th><?= number_format($total_advance);?></th>
                                    <th><?= number_format($total_debit);?></th>
                                    <th></th>
                                    <th><?= number_format($total_net_salary);?></th>
                                    <th><?= number_format($total_loan);?></th>
                                    <th><?= number_format($total_balance);?></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>

                    </div>
                </div>
            </div>
        </div>

    </div> <!-- end container -->
</div>
<div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Add Employee</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="<?php echo site_url('Manage_employee/addEmployee'); ?>" method="post" name="save" enctype="multipart/form-data">
                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">Full Name</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="text" id="name" name="name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="salary_amount" class="col-sm-4 col-form-label">Salary Amount</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="number" id="salary_amount" name="salary_amount">
                        </div>
                    </div><!--
                    <div class="form-group row">
                        <label for="example-date-input" class="col-sm-4 col-form-label">Date</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <input type="text" class="form-control"  id="mdate" name="date">
                            </div>
                        </div>
                    </div>-->
                    <input type="hidden" class="form-control"  id="delete" name="delete" value="0">
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" value="save">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>







            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- end wrapper -->
<?php $this->load->view("manage/footer"); ?>