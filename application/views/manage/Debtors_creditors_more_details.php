<!DOCTYPE html>
<html>
<?php $this->load->view("manage/header"); ?>


<body>

<!-- Loader -->
<div id="preloader"><div id="status"><div class="spinner"></div></div></div>

<?php $this->load->view("manage/topnav"); ?>


<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="#">KibahaOnline</a></li>
                            <li class="breadcrumb-item active">Employees</li>
                        </ol>
                    </div>
                    <?php
                     foreach ($getClient as $index => $item) {
                         ?>
                         <h4 class="page-title"><?=$item->name;?> Account's</h4>

                         <?php
                     }
 ?>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card m-b-30">
                    <div class="card-body">
                        <div class="text-right">

                                <button type="button" class="btn btn-primary waves-effect waves-light "
                                        data-toggle="modal" data-animation="bounce"
                                        data-target=".bs-example-modal-center-dr">Debit
                                </button>
                                <button type="button" class="btn btn-primary waves-effect waves-light "
                                        data-toggle="modal" data-animation="bounce"
                                        data-target=".bs-example-modal-center-cr">Credit
                                </button>

                             </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="card m-b-30">
                    <div class="card-body">
                        <!-- Nav tabs -->

                                            <ul class="nav nav-pills nav-justified" role="tablist">
                                                <li class="nav-item waves-effect waves-light">
                                                    <a class="nav-link active" data-toggle="tab" href="#home-1"
                                                       role="tab">Home</a>
                                                </li>
                                                <li class="nav-item waves-effect waves-light">
                                                    <a class="nav-link" data-toggle="tab" href="#profile-1" role="tab">Credit(Amount Paid)</a>
                                                </li>
                                                <li class="nav-item waves-effect waves-light">
                                                    <a class="nav-link" data-toggle="tab" href="#messages-1" role="tab">Debit(Amount Taken)</a>
                                                </li>
                                            </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active p-3" id="home-1" role="tabpanel">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Date</th>
                                        <th>Details</th>
                                        <th>Dr</th>
                                        <th>Cr</th>
                                        <th>Balance</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                  //  var_dump($getEmployee);
                                    if(isset($getPayment)){
                                        $sn=1;
                                        $salary=0;
                                        $total_salary=0;
                                        foreach ($getPayment as $index => $item) {
                                            //var_dump($item);
                                            $id=$item->id;
                                            //$pay_id=$item->payment_id;
                                            $balance=$item->balance;



                                            ?>
                                            <tr>
                                                <th scope="row"><?= $sn;?></th>
                                                <td ><?= $item->date;?></td>
                                                <td class="text-left"><?= $item->description;?></td>
                                                <td class="text-right"><?= number_format($item->dr);?></td>
                                                <td class="text-right"><?= number_format($item->cr);?></td>
                                                <td class="text-right"><?= number_format($item->balance);?></td>
                                                </tr>

                                            <!--         --><?php
                                            $sn++;}
                                        ?>



                                    </tbody>

                                    <?php
                                    }
                                    ?>
                                </table>
                            </div>
                            <div class="tab-pane p-3" id="profile-1" role="tabpanel">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Date</th>
                                        <th>Description</th>
                                        <th>Amount</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                     // var_dump($getAdvanceSalary);
                                    if(isset($getPaymentCr)){
                                        $sn=1;
                                        $cr=0;
                                        $total=0;
                                        foreach ($getPaymentCr as $index => $item) {
                                            //var_dump($item);
                                            $id=$item->id;
                                            $cr=$item->cr;
                                            $total=$total+$cr;
                                            ?>



                                            <tr>
                                                <th scope="row"><?= $sn;?></th>
                                                <td><?= $item->date;?></td>
                                                <td class="text-left"><?= $item->description;?></td>
                                                <td class="text-right"><?= number_format($item->cr);?></td>

                                                <td style="width: 100px;"><i class="fa fa-pencil-square-o btn-primary" data-toggle="tooltip" data-placement="top" title="edit"></i>&nbsp;&nbsp;&nbsp;<i class="fa fa-trash badge-danger" data-toggle="tooltip" data-placement="top" title="delete"></i></td>

                                            </tr>

                                            <!--         --><?php
                                            $sn++;}
                                    }
                                    ?>
                                    </tbody>
                                    <tfoot>
                                    <tr class="text-right">
                                        <th></th>
                                        <th>TOTAL</th>
                                        <th></th>
                                        <th><?= number_format($total);?></th>
                                        <th></th>

                                    </tr>
                                    </tfoot>
                                </table>

                            </div>
                            <div class="tab-pane p-3" id="messages-1" role="tabpanel">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Date</th>
                                        <th>Description</th>
                                        <th>Amount</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    // var_dump($getAdvanceSalary);
                                    if(isset($getPaymentDr)){
                                        $sn=1;
                                        $dr=0;
                                        $total=0;
                                        foreach ($getPaymentDr as $index => $item) {
                                            //var_dump($item);
                                            $id=$item->id;
                                            $dr=$item->dr;
                                            $total=$total+$dr;
                                            ?>



                                            <tr>
                                                <th scope="row"><?= $sn;?></th>
                                                <td><?= $item->date;?></td>
                                                <td class="text-left"><?= $item->description;?></td>
                                                <td class="text-right"><?= number_format($item->dr);?></td>

                                                <td style="width: 100px;"><i class="fa fa-pencil-square-o btn-primary" data-toggle="tooltip" data-placement="top" title="edit"></i>&nbsp;&nbsp;&nbsp;<i class="fa fa-trash badge-danger" data-toggle="tooltip" data-placement="top" title="delete"></i></td>

                                            </tr>

                                            <!--         --><?php
                                            $sn++;}
                                    }
                                    ?>
                                    </tbody>
                                    <tfoot>
                                    <tr class="text-right">
                                        <th></th>
                                        <th>TOTAL</th>
                                        <th></th>
                                        <th><?= number_format($total);?></th>
                                        <th></th>

                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="tab-pane p-3" id="settings-1" role="tabpanel">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Date</th>
                                        <th>Amount</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    // var_dump($getAdvanceSalary);
                                    if(isset($getPaySalary)){
                                        $sn=1;
                                        $pay_salary=0;
                                        $total=0;
                                        foreach ($getPaySalary as $index => $item) {
                                            //var_dump($item);
                                            $id=$item->id;
                                            $pay_salary=$item->amount;
                                            $total=$total+$pay_salary;
                                            ?>



                                            <tr>
                                                <th scope="row"><?= $sn;?></th>
                                                <td><?= $item->date;?></td>
                                                <td class="text-right"><?= number_format($pay_salary);?></td>

                                                <td style="width: 100px;"><i class="fa fa-pencil-square-o btn-primary" data-toggle="tooltip" data-placement="top" title="edit"></i>
                                                    <i class="fa fa-trash badge-danger" data-toggle="tooltip" data-placement="top" title="delete"></i></td>

                                            </tr>

                                            <!--         --><?php
                                            $sn++;}
                                    }
                                    ?>
                                    </tbody>
                                    <tfoot>
                                    <tr class="text-right">
                                        <th></th>
                                        <th>TOTAL</th>
                                        <th><?= number_format($total);?></th>
                                        <th></th>

                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="tab-pane p-3" id="settings-2" role="tabpanel">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Date</th>
                                        <th>description</th>
                                        <th>Loan Amount</th>
                                        <th>Monthly Deduction</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    // var_dump($getAdvanceSalary);
                                    if(isset($getLoan)){
                                        $sn=1;
                                        $loan=0;
                                        $total=0;
                                        foreach ($getLoan as $index => $item) {
                                            //var_dump($item);
                                            $id=$item->id;
                                            $loan=$item->amount;
                                            $total=$total+$loan;
                                            ?>



                                            <tr>
                                                <th scope="row"><?= $sn;?></th>
                                                <td><?= $item->date;?></td>
                                                <td><?= $item->description;?></td>
                                                <td class="text-right"><?= number_format($loan);?></td>
                                                <td class="text-right"><?= number_format($item->deduction);?></td>

                                                <td style="width: 100px;"><i class="fa fa-pencil-square-o btn-primary" data-toggle="tooltip" data-placement="top" title="edit"></i>
                                                    <i class="fa fa-trash badge-danger" data-toggle="tooltip" data-placement="top" title="delete"></i></td>

                                            </tr>

                                            <!--         --><?php
                                            $sn++;}
                                    }
                                    ?>
                                    </tbody>
                                    <tfoot>
                                    <tr class="text-right">
                                        <th></th>
                                        <th>TOTAL</th>
                                        <th></th>
                                        <th><?= number_format($total);?></th>
                                        <th></th>
                                        <th></th>

                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div> <!-- end container -->
</div>
<div class="modal fade bs-example-modal-center-dr" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Add Debit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="<?php echo site_url('debtors_creditors_more_details/addPaymentDr'); ?>" method="post" name="save" enctype="multipart/form-data">
                    <div class="form-group row">
                        <div class="col-sm-8">
                            <input class="form-control" type="hidden" id="client_id" name="client_id" value="<?php
                            foreach ($getClient as $index => $item) {
                                echo $item->id;
                            }

                            ?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="Description" class="col-sm-6 col-form-label">Description</label>
                        <div class="col-sm-6">
                            <input class="form-control" type="text" id="description" name="description">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="salary_amount" class="col-sm-6 col-form-label">Debit Amount</label>
                        <div class="col-sm-6">
                            <input class="form-control" type="number" id="dr" name="dr">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-date-input" class="col-sm-6 col-form-label">Date</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <input type="text" class="form-control mdate" name="date">
                            </div>
                        </div>
                    </div><!--
                    -->
                    <input type="hidden" class="form-control"  id="delete" name="delete" value="0">
                    <input type="hidden" class="form-control"  id="cr" name="cr" value="NULL">
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" value="save">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade bs-example-modal-center-cr" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Add Credit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form action="<?php echo site_url('debtors_creditors_more_details/addPaymentCr'); ?>" method="post" name="save" enctype="multipart/form-data">
                    <div class="form-group row">
                        <div class="col-sm-8">
                            <input class="form-control" type="hidden" id="client_id" name="client_id" value="<?php
                            foreach ($getClient as $index => $item) {
                                echo $item->id;
                            }

                            ?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="Description" class="col-sm-6 col-form-label">Description</label>
                        <div class="col-sm-6">
                            <input class="form-control" type="text" id="description" name="description">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="salary_amount" class="col-sm-6 col-form-label">Credit Amount</label>
                        <div class="col-sm-6">
                            <input class="form-control" type="number" id="cr" name="cr">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-date-input" class="col-sm-6 col-form-label">Date</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <input type="text" class="form-control mdate"   name="date">
                            </div>
                        </div>
                    </div><!--
                    -->
                    <input type="hidden" class="form-control"  id="delete" name="delete" value="0">
                    <input type="hidden" class="form-control"  id="dr" name="dr" value="NULL">
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" value="save">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- end wrapper -->
<?php $this->load->view("manage/footer"); ?>