<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>KibahaOnline</title>
    <meta content="Admin Dashboard" name="description" />
    <meta content="Mannatthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="<?=base_url('')?>assets/images/favicon.ico">

    <link href="<?=base_url('')?>assets/plugins/morris/morris.css" rel="stylesheet">

    <link href="<?=base_url('')?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url('')?>assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url('')?>assets/css/style.css" rel="stylesheet" type="text/css">

    <link href="<?=base_url('')?>assets/plugins/timepicker/tempusdominus-bootstrap-4.css" rel="stylesheet" />
    <link href="<?=base_url('')?>assets/plugins/timepicker/bootstrap-material-datetimepicker.css" rel="stylesheet">
    <link href="<?=base_url('')?>assets/plugins/clockpicker/jquery-clockpicker.min.css" rel="stylesheet" />
    <link href="<?=base_url('')?>assets/plugins/colorpicker/asColorPicker.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url('')?>assets/plugins/select2/select2.min.css" rel="stylesheet" type="text/css" />

    <link href="<?=base_url('')?>assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    <link href="<?=base_url('')?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="<?=base_url('')?>assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />


</head>
