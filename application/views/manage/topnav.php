<!-- Navigation Bar-->
<header id="topnav">

    <!-- end topbar-main -->

    <!-- MENU Start -->
    <div class="navbar-custom">
        <div class="container-fluid">
            <div id="navigation">
                <!-- Navigation Menu-->
                <ul class="navigation-menu">

                    <li class="has-submenu">
                        <a href="<?= base_url('Home') ?>"><i class="mdi mdi-airplay"></i>Dashboard</a>
                    </li>

                    <li class="has-submenu">
                        <a href="<?= base_url('Cash_submit') ?>"><i class="mdi mdi-layers"></i>Submit Cash</a>
                    </li>

                    <li class="has-submenu">
                        <a href="<?= base_url('Manage_employee') ?>"><i class="mdi mdi-bullseye"></i>Employee</a>
                    </li>

                    <li class="has-submenu">
                        <a href="<?= base_url('Debtors_creditors') ?>"><i class="mdi mdi-gauge"></i>Debtor/Creditor</a>
                    </li>

                    <!--                   <li class="has-submenu">
                                           <a href="#"><i class="mdi mdi-google-pages"></i>Pages</a>
                                           <ul class="submenu megamenu">
                                               <li>
                                                   <ul>
                                                       <li><a href="pages-login.html">Login</a></li>
                                                       <li><a href="pages-register.html">Register</a></li>
                                                       <li><a href="pages-recoverpw.html">Recover Password</a></li>
                                                       <li><a href="pages-lock-screen.html">Lock Screen</a></li>
                                                   </ul>
                                               </li>
                                               <li>
                                                   <ul>
                                                       <li><a href="pages-blank.html">Blank Page</a></li>
                                                       <li><a href="pages-404.html">Error 404</a></li>
                                                       <li><a href="pages-500.html">Error 500</a></li>
                                                   </ul>
                                               </li>
                                           </ul>
                                       </li>-->

                </ul>
                <!-- End navigation menu -->
            </div> <!-- end #navigation -->
        </div> <!-- end container -->
    </div> <!-- end navbar-custom -->
</header>
<!-- End Navigation Bar-->